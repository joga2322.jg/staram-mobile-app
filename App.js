import { SafeAreaProvider } from 'react-native-safe-area-context';
import BottomNavbar from './src/components/navbar/BottomNavbar';
import DashboardPage from './src/features/Dashboard/DashboardPage';
import LoginPage from './src/features/Login/LoginPage';
import ProductDetailPage from './src/features/Product/ProductDetail/ProductDetailPage';
import ProductListPage from './src/features/Product/ProductList/ProductListPage';
import RegistrationDetailPage from './src/features/Registration/RegistrationDetail/RegistrationDetailPage';
import RegistrationListPage from './src/features/Registration/RegistrationList/RegistrationListPage';
import SplashScreen from './src/features/SpashScreen/SpashScreen';
import { ThemeProvider } from './src/shared/context/ThemeContext';
import { useAppFont } from './src/shared/hook/useAppFont'

export default function App() {
  const fonts = useAppFont()
  if (!fonts){
    return null
  }
  return (
    <SafeAreaProvider>
      <ThemeProvider>
        {/* <LoginPage/> */}
        {/* <ProductListPage/> */}
        {/* <RegistrationDetailPage/> */}
        {/* <RegistrationListPage/> */}
        {/* <ProductDetailPage/> */}
        {/* <DashboardPage/> */}
        {/* <SplashScreen/> */}
        <BottomNavbar/>
      </ThemeProvider>
    </SafeAreaProvider>
  );
}