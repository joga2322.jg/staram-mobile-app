import { Image, StyleSheet, Text, View } from "react-native"
import { ButtonIcon } from "../../../components/basics/buttons/Buttons"
import CardContainer from "../../../components/basics/containers/CardContainer"
import MainContainer from "../../../components/basics/containers/MainContainer"
import { SearchInput } from "../../../components/basics/inputs/Inputs"
import { useTheme } from "../../../shared/context/ThemeContext"
import searchIcon from "../../../../assets/img/icon-search-white.png"
import ProductList from "./components/ProductList"
import useProductList from "./components/useProductList"

const ProductListPage = () => {
    const theme = useTheme()
    const styles = styling(theme)

    const { searchKey, setSearchKey, onSearch, filteredProducts, isKeyboardVisible, onProductClicked } = useProductList()
    return (
        <MainContainer>
            <View style={styles.container}>
                <Text style={[theme.text.h5Type, {color: theme.color.grey}]}>Product</Text>
                <View style={styles.productContainer}>
                    <View style={{flex: 1}}>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}]}>Product List</Text>
                        <CardContainer additionalStyle={{marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', zIndex: 10}}>
                            <SearchInput value={searchKey} onChangeValue={setSearchKey} placeholder="Search by Fund Name" />
                            <ButtonIcon onClick={onSearch}>
                                <Image source={searchIcon} style={{height: 24, width: 24}} />
                            </ButtonIcon>
                        </CardContainer>
                    </View>
                    {isKeyboardVisible ? 
                    <View style={{flex: 5, marginTop: 88}}>
                        <ProductList products={filteredProducts}/>
                    </View> :
                    <View style={{flex: 5, marginTop: 28}}>
                        <ProductList products={filteredProducts} onProductClicked={onProductClicked}/>
                    </View>
                    }
                </View>
            </View>
        </MainContainer>    
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        marginTop: 40,
        flex: 1,
    },
    productContainer: {
        marginTop: 24,
        marginBottom: 20,
        flex: 1
    }
})

export default ProductListPage