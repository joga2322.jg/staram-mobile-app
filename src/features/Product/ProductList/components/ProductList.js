import { FlatList, Text, TouchableOpacity } from "react-native"
import { BadgeContainer } from "../../../../components/basics/containers/BadgeContainer"
import CardContainer from "../../../../components/basics/containers/CardContainer"
import { useTheme } from "../../../../shared/context/ThemeContext"

const ProductList = ({products, onProductClicked}) => {
    const theme = useTheme()

    const renderProductItem = ({item}) => {
        return (
            <CardContainer additionalStyle={{paddingHorizontal: 16, paddingVertical: 12, marginVertical: 8, elevation: 8}}>
                <TouchableOpacity onPress={onProductClicked}>
                    <Text style={[theme.text.labelInput, theme.layout.mb6]}>Fund Name</Text>
                    <Text style={[theme.text.body1SemiboldType, theme.layout.mb6]}>{item.fund_name}</Text>
                    <Text style={[theme.text.labelInput, theme.layout.mb16]}>
                        Code: <Text style={{color: theme.color.orangePrimary}}>{item.fund_code}</Text>
                    </Text>
                    <BadgeContainer>
                        <Text style={[theme.text.hintType, {color: theme.color.lightGrey}]}>
                            Updated: <Text style={{color: theme.color.grey}}>{item.updatedTime}</Text>
                        </Text>
                    </BadgeContainer>
                </TouchableOpacity>
            </CardContainer>
        )
    }

    return (
        <FlatList
            data={products}
            renderItem={renderProductItem}
            keyExtractor={item => item.id}
            contentContainerStyle={{paddingHorizontal: 8}}
        />
    )
}

export default ProductList