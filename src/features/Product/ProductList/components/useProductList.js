import { useEffect, useState } from "react"
import { Keyboard } from "react-native"

const useProductList = () => {
    const products = [
        {
            "id": 1,
            "fund_name": "STAR Sharia Money Market",
            "fund_code": "100",
            "updatedTime": "01/01/2008,00:00:01"
        },
        {
            "id": 2,
            "fund_name": "STAR Money Market",
            "fund_code": "100",
            "updatedTime": "01/01/2008,00:00:01"
        },
        {
            "id": 3,
            "fund_name": "STAR USD Money Market",
            "fund_code": "100",
            "updatedTime": "01/01/2008,00:00:01"
        },
        {
            "id": 4,
            "fund_name": "STAR Sharia Money Market",
            "fund_code": "100",
            "updatedTime": "01/01/2008,00:00:01"
        },
        {
            "id": 5,
            "fund_name": "STAR Sharia Money Market",
            "fund_code": "100",
            "updatedTime": "01/01/2008,00:00:01"
        }
    ]

    const [searchKey, setSearchKey] = useState('')
    const [filteredProducts, setFilteredProduct] = useState([])

    const [isKeyboardVisible, setKeyboardVisible] = useState(false);

    useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
        'keyboardDidShow',
        () => {
            setKeyboardVisible(true); // or some other action
        }
        );
        const keyboardDidHideListener = Keyboard.addListener(
        'keyboardDidHide',
        () => {
            setKeyboardVisible(false); // or some other action
        }
        );

        return () => {
        keyboardDidHideListener.remove();
        keyboardDidShowListener.remove();
        };
    }, []);

    useEffect(() => {
        setFilteredProduct(products)
    }, [])

    const onSearch = () => {
        if (searchKey) {
            const filteredItems = filteredProducts.filter(item => item.fund_name.includes(searchKey))
            setFilteredProduct(filteredItems)
        } else{
            setFilteredProduct(products)
        }
    }

    const onProductClicked = () => {

    }

    return {
        searchKey,
        setSearchKey,
        onSearch,
        filteredProducts,
        isKeyboardVisible,
        onProductClicked
    }
}

export default useProductList