import { useState } from "react";

const useProductDetail = () => {
    const [product, setProduct] = useState({})

    return {
        product
    }
}
 
export default useProductDetail;