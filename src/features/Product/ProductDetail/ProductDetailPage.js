import { useTheme } from "../../../shared/context/ThemeContext"
import { Dimensions, Image, ScrollView, StyleSheet, Text, View } from "react-native";
import CardContainer from "../../../components/basics/containers/CardContainer";
import MainContainer from "../../../components/basics/containers/MainContainer";
import { ButtonIconTransparent, ButtonPrimary } from "../../../components/basics/buttons/Buttons";
import useProductDetail from "./useProductDetail";
import searchIcon from "../../../../assets/img/icon-back-grey.png"

const ProductDetailPage = () => {
    const theme = useTheme()
    const styles = styling(theme)
    const {product} = useProductDetail()
    const screenWidth = 0.9*Dimensions.get('window').width;
    return ( 
        <MainContainer>
            <View style={[styles.container]} >
            <View style={[{width: "100%", flexDirection:'row', alignItems:'center', height:'auto'},styles.mb8]}>
                <ButtonIconTransparent>
                    <Image source={searchIcon} style={{height: 32, width: 32}} />
                </ButtonIconTransparent>
                <Text style={[theme.text.h5Type, {color: theme.color.grey, marginLeft:8}]}>Product Detail</Text>
            </View>
            <ScrollView contentContainerStyle={{width: screenWidth, paddingHorizontal:12, paddingTop: 12}}>
                <CardContainer additionalStyle={[{flexDirection: 'column',width:'100%', padding:16}, styles.mb24]}>
                    <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Fund Name</Text>
                    <Text style={[theme.text.body1SemiboldType, {color: theme.color.grey}, styles.mb8]}>STAR Sharia Money Market</Text>
                    <Text style={[theme.text.labelInput, {color: theme.color.extraLightGrey}, styles.mb24]}>Fund Code: <Text style={[{color: theme.color.orangePrimary}]}>10 </Text></Text>
                    <View style={styles.containerBadge}>
                        <Text style={[theme.text.labelInput, {color: theme.color.extraLightGrey}]}>
                            Updated:{' '}
                            <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}]}>
                                17 August 2022 14:58:55
                            </Text>
                        </Text>
                    </View>
                </CardContainer>
                <View style={[styles.flexRow, styles.mb24]}>
                    <View style={[{padding: 16}, styles.flexColumn, styles.startCenter, styles.whiteBG, styles.marginRight6, styles.cardElevation, styles.borderRadius10]}>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}]}>
                            ID:{' '}
                            <Text style={[theme.text.labelInput, {color: theme.color.orangePrimary}]}>
                                10
                            </Text>
                        </Text>
                    </View>
                    <View style={[{padding: 16},styles.flexColumn, styles.startCenter, styles.whiteBG, styles.marginLeft6, styles.cardElevation, styles.borderRadius10]}>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}]}>
                            Prefix:{' '}
                            <Text style={[theme.text.labelInput, {color: theme.color.orangePrimary}]}>
                                ST
                            </Text>
                        </Text>
                    </View>
                </View>
                <CardContainer additionalStyle={[{flexDirection: 'column',width:'100%', padding:16}, styles.mb24]}>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb24]}>
                        Fund Allocation
                    </Text>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb8]}>
                        Stock - 
                        <Text style={[{color: theme.color.grey}, styles.mb8]}>
                            {' '}60%
                        </Text>
                    </Text>
                    <View style={[{width: "100%"}, styles.mb16, styles.containerProgressBar]}>
                        <View style={[{width:'60%'}, styles.containerProgressBarFill]}/>
                    </View>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb8]}>
                        Forex - 
                        <Text style={[{color: theme.color.grey}, styles.mb8]}>
                            {' '}30%
                        </Text>
                    </Text>
                    <View style={[{width: "100%"}, styles.mb16, styles.containerProgressBar]}>
                        <View style={[{width:'30%'}, styles.containerProgressBarFill]}/>
                    </View>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb8]}>
                        Obligation -
                        <Text style={[{color: theme.color.grey}, styles.mb8]}>
                            {' '}10%
                        </Text>
                    </Text>
                    <View style={[{width: "100%"}, styles.mb16, styles.containerProgressBar]}>
                        <View style={[{width:'10%'}, styles.containerProgressBarFill]}/>
                    </View>
                </CardContainer>
                <CardContainer additionalStyle={[{flexDirection: 'column',width:'100%', padding:16}, styles.mb24]}>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb24]}>
                        Fund Documents
                    </Text>
                    <View style={[styles.mb24, styles.containerDocument]}>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Fund Fact ID</Text>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.grey}, styles.mb24]}>this-is-fund-fact-id-number0001</Text>
                        <ButtonPrimary label="View Fund Fact Sheet" onClick={() => {}}/>
                    </View>
                    <View style={[styles.containerDocument]}>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Prospectus ID</Text>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.grey}, styles.mb24]}>this-is-prospectus-id-number0001</Text>
                        <ButtonPrimary label="View Prospectus" onClick={() => {}}/>
                    </View>                    
                </CardContainer>
            </ScrollView>
            </View>
        </MainContainer>
    );
}

const styling = (theme) => StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
        height: '100%',
        marginTop: 40,
    },
    containerInside: {
        flexDirection: "column",
        alignItems: "flex-start",
        width: "auto"
    },
    containerProgressBar :{
        flex:1, 
        height:8,
        backgroundColor: theme.color.orangeExtraLight,
        borderRadius: 5
    },
    containerProgressBarFill :{
        flex:1, 
        height:8,
        backgroundColor: theme.color.orangePrimary,
        borderRadius: 5
    },
    containerDocument:{
        width: '100%', 
        borderRadius: 10,
        borderStyle: "dashed",
        borderColor: "#9d9d9d",
        borderWidth: 1,
        padding: 16
    },
    containerBadge: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: theme.color.orangeExtraLight,
        paddingVertical: 8,
        borderRadius: 50
    },
    detailContainer: {
        marginHorizontal: 12,
        display: "flex",
        flexDirection: "column",
        padding: 16,
        backgroundColor: theme.color.white,
        borderRadius: 10,
        alignSelf: 'stretch',

        shadowColor: 'rgba(133, 133, 133, 1)',
        shadowOffset: {width: 6, height: 6},
        elevation: 15
    },
    titleContainer: {
        marginBottom: 32,
        alignItems: 'center',
        display: "flex"
    },
    mb8:{
        marginBottom: 8,
    },
    mb16:{
        marginBottom: 16,
    },
    mb32:{
        marginBottom: 32,
    },
    mb24:{
        marginBottom: 24,
    },
    padding16:{
        padding:16
    },
    marginHorizontal12: {
        marginHorizontal: 12
    },
    marginLeft6:{
        marginLeft: 6
    },
    marginRight6:{
        marginRight: 6
    },
    borderRadius10: {
        borderRadius: 10
    },
    cardElevation: {
        shadowColor: '#D3D3D3',
        shadowOffset: {width: 4, height: 4},
        elevation: 15
    },
    flexRow: {
        flex: 1,
        flexDirection: 'row'
    },
    flexColumn: {
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    startCenter: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    whiteBG:{
        backgroundColor: 'white'
    },
    greenBadge: {
        backgroundColor: '#DCFBE3',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    redBadge: {
        backgroundColor: '#FBDCDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    blueBadge: {
        backgroundColor: '#ECF6FF',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    yellowBadge: {
        backgroundColor: '#FBEDDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    }
})
 
export default ProductDetailPage;