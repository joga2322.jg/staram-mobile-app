import { Image, ImageBackground, StyleSheet, Text, View } from "react-native";
import MainContainer from "../../components/basics/containers/MainContainer";
import image from "../../../assets/img/spashBackground.png"
import starAM from "../../../assets/img/logo-star-am.png"

const SplashScreen = () => {
    return ( 
        <View style={styles.container}>
            <ImageBackground source={image} resizeMode="cover" style={styles.image}>
                <View >
                    <Image source={starAM} style={{height: 85.4, width: 240}}/>
                </View>
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    image: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    }
  });
 
export default SplashScreen;