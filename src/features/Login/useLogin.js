import { useState } from "react"

const useLogin = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    return {
        username,
        setUsername,
        password,
        setPassword
    }
}

export default useLogin