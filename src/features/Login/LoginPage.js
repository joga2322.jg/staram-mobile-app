import { StyleSheet, Text, View } from "react-native"
import { ButtonPrimary } from "../../components/basics/buttons/Buttons"
import CardContainer from "../../components/basics/containers/CardContainer"
import MainContainer from "../../components/basics/containers/MainContainer"
import { AppPasswordInput, AppTextInput } from "../../components/basics/inputs/Inputs"
import { useTheme } from "../../shared/context/ThemeContext"
import useLogin from "./useLogin"

const LoginPage = () => {
    const {username, setUsername, password, setPassword} = useLogin()

    const theme = useTheme()
    const styles = styling(theme)
    return (
        <MainContainer>
            <View style={styles.container}>
                <CardContainer additionalStyle={styles.loginContainer}>
                    <View style={styles.titleContainer}>
                        <Text style={[theme.text.h5Type, {color: theme.color.grey}]}>Login</Text>
                        <Text style={[theme.text.body2Type, {color: theme.color.lightGrey}]}>Access To Your Account</Text>
                    </View>
                    <AppTextInput label="Username" value={username} onChangeValue={setUsername} placeholder="Input username" />
                    <View style={{height: 20}} />
                    <AppPasswordInput value={password} onChangeValue={setPassword} placeholder="Input password" />
                    <View style={{height: 40}} />
                    <ButtonPrimary label="Login" onClick={() => {}} />
                </CardContainer>
            </View>
            
        </MainContainer>
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        justifyContent: 'center',
        height: '100%',
        marginHorizontal: 20
    },
    loginContainer: {
        paddingHorizontal: 20,
        paddingTop: 28,
        paddingBottom: 40
    },
    titleContainer: {
        marginBottom: 32,
        alignItems: 'center',
        display: "flex"
    }
})

export default LoginPage
