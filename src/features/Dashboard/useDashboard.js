import { useEffect, useState } from "react";

const useDashboard = () => {
    const uploadSummary = {
        "isDailyNavUploaded": false,
        "isDailyRegisUploaded": false,
        "isDailyTrxUploaded": false,
        "isDailyBalanceUploaded": false
    }

    const businessSummary = {
        "totalTransactionSub": 23533,
        "totalTransactionRedeem": 23411,
        "totalRegistration": 12334,
        "totalProduct": 800
    }

    const [filterEnabled, setFilterEnabled] = useState(false)
    const [startDateDisplay, setStartDateDisplay] = useState(false)
    const [endDateDisplay, setEndDateDisplay] = useState(false)
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState(new Date())
    const [isDatePicked, setDatePicked] = useState({
        startDate: false,
        endDate: false
    })
    
    return {
        uploadSummary,
        businessSummary,
        filterEnabled,
        setFilterEnabled,
        startDateDisplay,
        setStartDateDisplay,
        startDate, 
        setStartDate,
        isDatePicked, 
        setDatePicked,
        endDate, 
        setEndDate,
        endDateDisplay, 
        setEndDateDisplay
    }
}
 
export default useDashboard;