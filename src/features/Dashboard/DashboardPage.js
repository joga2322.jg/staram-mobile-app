import { Dimensions, ScrollView, StyleSheet, Text, View, Image, Modal, Pressable, TextInput } from "react-native";
import { ButtonIconTransparent, ButtonPrimary } from "../../components/basics/buttons/Buttons";
import CardContainer from "../../components/basics/containers/CardContainer";
import MainContainer from "../../components/basics/containers/MainContainer";
import { useTheme } from "../../shared/context/ThemeContext";
import iconNAV from "../../../assets/img/icon-diamond.png"
import iconREG from "../../../assets/img/icon-register.png"
import iconWallet from "../../../assets/img/icon-wallet.png"
import iconTRX from "../../../assets/img/icon-transaction.png"
import iconDashboard from "../../../assets/img/icon-dashboard.png"
import iconACT from "../../../assets/img/icon-active-investor.png"
import iconFund from "../../../assets/img/icon-mutual-fund.png"
import iconClose from "../../../assets/img/icon-close-light-grey.png"
import iconDate from "../../../assets/img/icon-date.png"
import useDashboard from "./useDashboard";
import DateTimePicker from '@react-native-community/datetimepicker'
import { useState } from "react";

const screenWidth = 0.90*Dimensions.get('window').width;
const screenheight = 0.70*Dimensions.get('window').height;

const DashboardPage = () => {
    const {
        uploadSummary,
        businessSummary,
        filterEnabled,
        setFilterEnabled,
        setStartDateDisplay,
        startDateDisplay,
        startDate, 
        setStartDate,
        isDatePicked, 
        setDatePicked,
        endDate, 
        setEndDate,
        endDateDisplay, 
        setEndDateDisplay
    } = useDashboard()
    const theme = useTheme()
    const styles = styling(theme)
    return ( 
        <MainContainer>
            <Modal
                animationType="slide"
                transparent={true}
                visible={filterEnabled}
                onRequestClose={() => {
                setFilterEnabled(!filterEnabled);
                }}
            >
                <View style={{top: 0, bottom: 0, left:0, right: 0, position:'absolute', backgroundColor:'rgba(0,0,0,0.3)', flex: 1, alignItems:'center', justifyContent:'flex-end'}}>
                    <View style={{width:'100%', backgroundColor: 'white', padding: 24, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:"center", marginBottom:24}}>
                            <Text style={[theme.text.h5Type, {color: theme.color.Grey}]}>Date Filter</Text>
                            <Pressable
                            onPress={() => setFilterEnabled(!filterEnabled)}
                            >
                                <Image source={iconClose} style={{width:20,height:20}}/>
                            </Pressable>
                        </View>
                        <Text style={[styles.mb8, {color: theme.color.grey}]}>Start Date</Text>
                        <Pressable
                        style={[styles.mb16]}
                        onPress={() => setStartDateDisplay(true)}
                        >
                            <View style={{flexDirection:'row', alignItems: "center", width: '100%', borderWidth: 1, borderColor: theme.color.extraLightGrey, padding: 16, borderRadius: 20}}>
                                <Image source={iconDate} style={{width: 24, height:24, marginRight: 12}}/>
                                {!isDatePicked.startDate ? 
                                    <Text style={{color: theme.color.lightGrey}}>Please select a date</Text> :
                                    <Text>{`${startDate.getDate()}/${startDate.getMonth()}/${startDate.getFullYear()}`}</Text>
                                }
                            </View>
                        </Pressable>
                        {startDateDisplay ? 
                            <DateTimePicker value={startDate} onChange={
                                (event,value) => {[setStartDateDisplay(false),setStartDate(value),setDatePicked(prev=>({...prev, "startDate": true}))]}
                            }/>:
                            <></>
                        }

                        <Text style={[styles.mb8, {color: theme.color.grey}]}>End Date</Text>
                        <Pressable
                        style={[styles.mb32]}
                        onPress={() => {if (isDatePicked.startDate ) {
                            setEndDateDisplay(true)
                        }}}
                        >
                            <View style={{flexDirection:'row', alignItems: "center", width: '100%', borderColor: theme.color.extraLightGrey, borderWidth: 1, padding: 16, borderRadius: 20}}>
                                <Image source={iconDate} style={{width: 24, height:24, marginRight: 12}}/>
                                {!isDatePicked.endDate ? 
                                    <Text style={{color: theme.color.lightGrey}}>Please select a date</Text> :
                                    <Text>{`${endDate.getDate()}/${endDate.getMonth()}/${endDate.getFullYear()}`}</Text>
                                }
                            </View>
                        </Pressable>
                        {endDateDisplay ? 
                            <DateTimePicker minimumDate={startDate} value={endDate} onChange={
                                (event,value) => {[setEndDateDisplay(false),setEndDate(value),setDatePicked(prev=>({...prev, "endDate": true}))]}
                            }/>:
                            <></>
                        }
                        <ButtonPrimary label="Apply Filter" onClick={()=>{[setFilterEnabled(false)]}}/>
                    </View>
                </View>
            </Modal>
            <View style={[styles.container]} >
                <Text style={[theme.text.h5Type, {color: theme.color.grey}, styles.mb24]}>Dashboard</Text>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.mb24}>
                    <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb16]}>Today's Upload Summary</Text>
                    <View style={{height: 100}}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{height: 100}}>
                            <View style={[styles.cardElevation,styles.uploadHistoryCard]}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={styles.uploadHistoryImage}>
                                        <Image source={iconNAV} style={{width: 24, height: 24}}></Image>
                                    </View>
                                    <View style={{width: '70%'}}>
                                        <Text style={[theme.text.body2SemiboldType,styles.mb8,{color: theme.color.grey}]}>Latest NAV File</Text>
                                        {uploadSummary.isDailyNavUploaded === true ? 
                                            <Text style={[styles.greenBadge, {color: theme.color.greenBadge, width: '65%'}]}>Uploaded</Text>:
                                            <Text style={[styles.redBadge, {color: theme.color.redBadge, width: '70%'}]}>No Upload</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.cardElevation,styles.uploadHistoryCard]}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={styles.uploadHistoryImage}>
                                        <Image source={iconREG} style={{width: 24, height: 24}}></Image>
                                    </View>
                                    <View style={{width: '70%'}}>
                                        <Text style={[theme.text.body2SemiboldType,styles.mb8,{color: theme.color.grey}]}>Registration File</Text>
                                        {uploadSummary.isDailyRegisUploaded === true ? 
                                            <Text style={[styles.greenBadge, {color: theme.color.greenBadge, width: '65%'}]}>Uploaded</Text>:
                                            <Text style={[styles.redBadge, {color: theme.color.redBadge, width: '70%'}]}>No Upload</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.cardElevation,styles.uploadHistoryCard]}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={styles.uploadHistoryImage}>
                                        <Image source={iconWallet} style={{width: 24, height: 24}}></Image>
                                    </View>
                                    <View style={{width: '70%'}}>
                                        <Text style={[theme.text.body2SemiboldType,styles.mb8,{color: theme.color.grey}]}>User Balance File</Text>
                                        {uploadSummary.isDailyBalanceUploaded === true ? 
                                            <Text style={[styles.greenBadge, {color: theme.color.greenBadge, width: '65%'}]}>Uploaded</Text>:
                                            <Text style={[styles.redBadge, {color: theme.color.redBadge, width: '70%'}]}>No Upload</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.cardElevation,styles.uploadHistoryCard]}>
                                <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                    <View style={styles.uploadHistoryImage}>
                                        <Image source={iconTRX} style={{width: 24, height: 24}}></Image>
                                    </View>
                                    <View style={{width: '70%'}}>
                                        <Text style={[theme.text.body2SemiboldType,styles.mb8,{color: theme.color.grey}]}>Transaction File</Text>
                                        {uploadSummary.isDailyBalanceUploaded === true ? 
                                            <Text style={[styles.greenBadge, {color: theme.color.greenBadge, width: '65%'}]}>Uploaded</Text>:
                                            <Text style={[styles.redBadge, {color: theme.color.redBadge, width: '70%'}]}>No Upload</Text>
                                        }
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </View>
                <View style={{marginBottom:40}}>
                    <View style={{justifyContent:'space-between', flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb16]}>Business Summary</Text>
                        <Text style={[theme.text.body2Type, {color: theme.color.orangePrimary}, styles.mb16]} onPress={()=>setFilterEnabled(true)}>Filter</Text>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{width: screenWidth, paddingHorizontal:12}}>
                        <CardContainer additionalStyle={[{padding:16}, styles.mb16]}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.uploadHistoryImage}>
                                    <Image source={iconTRX} style={{width: 24, height: 24}}></Image>
                                </View>
                                <View style={{marginBottom:8}}>
                                    <Text style={[theme.text.h5Type,{color: theme.color.grey}]}>{businessSummary.totalTransactionSub}</Text>
                                    <Text style={[theme.text.labelInput,{color: theme.color.lightGrey}]}>Total Sub Transactions</Text>
                                </View>
                            </View>
                        </CardContainer>
                        <CardContainer additionalStyle={[{padding:16}, styles.mb16]}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.uploadHistoryImage}>
                                    <Image source={iconDashboard} style={{width: 24, height: 24}}></Image>
                                </View>
                                <View style={{marginBottom:8}}>
                                    <Text style={[theme.text.h5Type,{color: theme.color.grey}]}>{businessSummary.totalTransactionRedeem}</Text>
                                    <Text style={[theme.text.labelInput,{color: theme.color.lightGrey}]}>Total Redeem Transactions</Text>
                                </View>
                            </View>
                        </CardContainer>
                        <CardContainer additionalStyle={[{padding:16}, styles.mb16]}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.uploadHistoryImage}>
                                    <Image source={iconACT} style={{width: 24, height: 24}}></Image>
                                </View>
                                <View style={{marginBottom:8}}>
                                    <Text style={[theme.text.h5Type,{color: theme.color.grey}]}>{businessSummary.totalRegistration}</Text>
                                    <Text style={[theme.text.labelInput,{color: theme.color.lightGrey}]}>Total Registrations</Text>
                                </View>
                            </View>
                        </CardContainer>
                        <CardContainer additionalStyle={[{padding:16}, styles.mb16]}>
                            <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
                                <View style={styles.uploadHistoryImage}>
                                    <Image source={iconFund} style={{width: 24, height: 24}}></Image>
                                </View>
                                <View style={{marginBottom:8}}>
                                    <Text style={[theme.text.h5Type,{color: theme.color.grey}]}>{businessSummary.totalProduct}</Text>
                                    <Text style={[theme.text.labelInput,{color: theme.color.lightGrey}]}>Total Products</Text>
                                </View>
                            </View>
                        </CardContainer>
                    </ScrollView>
                </View>
                </ScrollView>
            </View>
        </MainContainer>
    );
}

const styling = (theme) => StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
        height: '100%',
        marginTop: 40,
    },
    containerInside: {
        flexDirection: "column",
        alignItems: "flex-start",
        width: "auto"
    },
    uploadHistoryCard:{
        borderStyle: "dashed", 
        borderRadius: 10, 
        borderWidth: 1, 
        borderColor: '#9d9d9d', 
        width: 260, 
        padding: 16, 
        marginRight: 16,
        backgroundColor: 'white' 
    },
    uploadHistoryImage:{
        backgroundColor: theme.color.orangeExtraLight, 
        height:48, 
        width:48, 
        alignItems:"center", 
        justifyContent:"center",
        borderRadius: 24,
        marginRight: 16
    },
    mb8:{
        marginBottom: 8,
    },
    mb16:{
        marginBottom: 16,
    },
    mb32:{
        marginBottom: 32,
    },
    mb24:{
        marginBottom: 24,
    },
    padding16:{
        padding:16
    },
    marginHorizontal12: {
        marginHorizontal: 12
    },
    marginLeft6:{
        marginLeft: 6
    },
    marginRight6:{
        marginRight: 6
    },
    borderRadius10: {
        borderRadius: 10
    },
    cardElevation: {
        shadowColor: '#D3D3D3',
        shadowOffset: {width: 4, height: 4},
        elevation: 15
    },
    flexRow: {
        flex: 1,
        flexDirection: 'row'
    },
    flexColumn: {
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    whiteBG:{
        backgroundColor: 'white'
    },
    greenBadge: {
        backgroundColor: '#DCFBE3',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    redBadge: {
        backgroundColor: '#FBDCDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    blueBadge: {
        backgroundColor: '#ECF6FF',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    yellowBadge: {
        backgroundColor: '#FBEDDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    }
})
 
export default DashboardPage;