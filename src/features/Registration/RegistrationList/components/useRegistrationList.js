import { useEffect, useState } from "react"
import { Keyboard } from "react-native";

const useRegistrationList = () => {
    const [currentPage, setCurrentPage] = useState(1);
    const [maxPage, setMaxPage] = useState(0);
    const [isNext, setIsNext] = useState(true);
    const [registrations, setRegistrations] = useState([])
    const [isLoading, setLoading] = useState(false)

    const [searchKey, setSearchKey] = useState('')
    const [isKeyboardVisible, setKeyboardVisible] = useState(false)

    const response = {
        data: [
            {
                "id": 1,
                "kyc_name": "dummy name",
                "registrationStatus": "ACT",
                "sid": "dummy sid",
                "ifua": "dummy ifua",
                "completion_status": "COMPLETED",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 2,
                "kyc_name": "dummy name",
                "registrationStatus": "REJ",
                "sid": null,
                "ifua": null,
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 3,
                "kyc_name": "dummy name",
                "registrationStatus": "PEN",
                "sid": "dummy sid",
                "ifua": "dummy ifua",
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 4,
                "kyc_name": "dummy name",
                "registrationStatus": "REJ",
                "sid": null,
                "ifua": null,
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 5,
                "kyc_name": "dummy name",
                "registrationStatus": "ACT",
                "sid": "dummy sid",
                "ifua": "dummy ifua",
                "completion_status": "COMPLETED",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 6,
                "kyc_name": "dummy name",
                "registrationStatus": "REJ",
                "sid": null,
                "ifua": null,
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 7,
                "kyc_name": "dummy name",
                "registrationStatus": "ACT",
                "sid": "dummy sid",
                "ifua": "dummy ifua",
                "completion_status": "COMPLETED",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 8,
                "kyc_name": "dummy name",
                "registrationStatus": "REJ",
                "sid": null,
                "ifua": null,
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 9,
                "kyc_name": "dummy name",
                "registrationStatus": "ACT",
                "sid": "dummy sid",
                "ifua": "dummy ifua",
                "completion_status": "COMPLETED",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            },
            {
                "id": 10,
                "kyc_name": "dummy name",
                "registrationStatus": "REJ",
                "sid": null,
                "ifua": null,
                "completion_status": "WAITING",
                "reject_reason": null,
                "updatedTime": "01/01/2008,00:00:01",
                "createdTime": "01/01/2008,00:00:01"
            }
        ],
        totalRow: 10
    }

    useEffect(() => {
        onGetAllRegistrations()
    }, [currentPage])

    useEffect(() => {
        onGetAllRegistrations()

        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow', () => {
                setKeyboardVisible(true); // or some other action
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide', () => {
                setKeyboardVisible(false); // or some other action
            }
        );
    
        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, [])

    const onGetAllRegistrations = async () => {
        setLoading(true)
        setTimeout(() => {
            if(currentPage == 1){
                setRegistrations(response.data)
                setMaxPage(response.totalRow/10) // the number 10 here is constant because every fetch process is returning data with limit of 10
            } else{
                setRegistrations([...registrations, response.data])
            }

            if (currentPage < maxPage){
                setIsNext(true)
            } else{
                setIsNext(false)
            }
            setLoading(false)
        }, 1500);
        
    }
    
    const onFetchMore = () => {
        if (isNext){
            setCurrentPage(prev => prev+1)
        } else{
            onGetAllRegistrations()
        }
    }

    const onRefresh = () => {
        if (currentPage !== 1) {
            setCurrentPage(1)
        } else{
            onGetAllRegistrations()
        }
    }

    const onRegistrationClicked = () => {

    }

    const onSearch = () => {

    }

    return {
        registrations,
        onRefresh,
        onFetchMore,
        onRegistrationClicked,
        isLoading,
        isKeyboardVisible,
        searchKey,
        setSearchKey,
        onSearch
    }
}

export default useRegistrationList