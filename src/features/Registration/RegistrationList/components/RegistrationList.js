import { FlatList, Text, TouchableOpacity, View } from "react-native"
import { BadgeContainer, MiniBadge } from "../../../../components/basics/containers/BadgeContainer"
import CardContainer from "../../../../components/basics/containers/CardContainer"
import { useTheme } from "../../../../shared/context/ThemeContext"

const RegistrationList = ({registrations, onRegistrationClicked, onRefresh, onFetchMore, isLoading}) => {
    const theme = useTheme()

    const getMapStatus = (status) => {
        switch (status) {
            case "PEN":
                return "PENDING"
            case "REJ":
                return "REJECTED"
            case "ACT":
                return "ACCEPTED"
            default:
                break;
        }
    }

    const renderRegistrationItem = ({item}) => {
        return (
            <CardContainer additionalStyle={{paddingHorizontal: 16, paddingVertical: 12, marginVertical: 8, elevation: 8}}>
                <TouchableOpacity onPress={onRegistrationClicked}>
                    <View>
                        <Text style={[theme.text.labelInput, theme.layout.mb6, {color: theme.color.lightGrey}]}>Customer Name</Text>
                        <Text style={[theme.text.body1SemiboldType, theme.layout.mb6, {color: theme.color.grey}]}>{item.kyc_name}</Text>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}]}>
                            SID: <Text style={{color: theme.color.orangePrimary}}>{item.sid}</Text>
                        </Text>
                    </View>
                    <View style={theme.layout.mv24}>
                        <Text style={[theme.text.labelInput, theme.layout.mb8, {color: theme.color.lightGrey}]}>Registration Status</Text>
                        {item.registrationStatus === "PEN" && <MiniBadge type="warning" label={getMapStatus(item.registrationStatus)} />}
                        {item.registrationStatus === "REJ" && <MiniBadge type="danger" label={getMapStatus(item.registrationStatus)} />}
                        {item.registrationStatus === "ACT" && <MiniBadge type="success" label={getMapStatus(item.registrationStatus)} />}
                    </View>
                    <BadgeContainer>
                        <Text style={[theme.text.hintType, {color: theme.color.extraLightGrey}]}>
                            Updated: <Text style={{color: theme.color.lightGrey}}>{item.updatedTime}</Text>
                        </Text>
                    </BadgeContainer>
                </TouchableOpacity>
            </CardContainer>
        )
    }

    return (
        <FlatList
            data={registrations}
            renderItem={renderRegistrationItem}
            keyExtractor={item => item.id}
            contentContainerStyle={{paddingHorizontal: 8}}
            onRefresh={onRefresh} refreshing={isLoading}
            onEndReached={onFetchMore}
        />
    )
}

export default RegistrationList