import { Image, StyleSheet, Text, View } from "react-native"
import CardContainer from "../../../components/basics/containers/CardContainer"
import MainContainer from "../../../components/basics/containers/MainContainer"
import { SearchInput } from "../../../components/basics/inputs/Inputs"
import { useTheme } from "../../../shared/context/ThemeContext"
import searchIcon from "../../../../assets/img/icon-search-white.png"
import RegistrationList from "./components/RegistrationList"
import useRegistrationList from "./components/useRegistrationList"
import { ButtonIcon } from "../../../components/basics/buttons/Buttons"

const RegistrationListPage = () => {
    const theme = useTheme()
    const styles = styling(theme)

    const { registrations, onRefresh, onFetchMore, onRegistrationClicked, isLoading, isKeyboardVisible, searchKey, setSearchKey, onSearch } = useRegistrationList()

    return (
        <MainContainer>
            <View style={styles.container}>
                <Text style={[theme.text.h5Type, {color: theme.color.grey}]}>Registration</Text>
                <View style={styles.registrationContainer}>
                    <View style={{flex: 1}}>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}]}>Registration List</Text>
                        <CardContainer additionalStyle={{marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', zIndex: 10}}>
                            <SearchInput value={searchKey} onChangeValue={setSearchKey} placeholder="Search by Customer Name" />
                            <ButtonIcon onClick={onSearch}>
                                <Image source={searchIcon} style={{height: 24, width: 24}} />
                            </ButtonIcon>
                        </CardContainer>
                    </View>
                    {isKeyboardVisible ? 
                    <View style={{flex: 5, marginTop: 88}}>
                        <RegistrationList registrations={registrations} onRegistrationClicked={onRegistrationClicked} onRefresh={onRefresh} onFetchMore={onFetchMore} isLoading={isLoading} />
                    </View> :
                    <View style={{flex: 5, marginTop: 28}}>
                        <RegistrationList registrations={registrations} onRegistrationClicked={onRegistrationClicked} onRefresh={onRefresh} onFetchMore={onFetchMore} isLoading={isLoading} />
                    </View>
                    }
                </View>
            </View>
        </MainContainer>    
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        marginTop: 40,
        flex: 1,
    },
    registrationContainer: {
        marginTop: 24,
        marginBottom: 20,
        flex: 1
    }
})

export default RegistrationListPage