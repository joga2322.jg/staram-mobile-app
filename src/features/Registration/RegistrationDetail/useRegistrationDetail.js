import { useState } from "react"

const useRegistrationDetail = () => {
    const [registrant, setRegistrant] = useState({})

    return {
        registrant
    }
}

export default useRegistrationDetail