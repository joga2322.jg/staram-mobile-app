import { Image, ScrollView, StyleSheet, Text, View } from "react-native"
import MainContainer from "../../../components/basics/containers/MainContainer"
import { useTheme } from "../../../shared/context/ThemeContext"
import { Dimensions } from 'react-native';
import useRegistrationDetail from "./useRegistrationDetail"
import { ButtonIconTransparent } from "../../../components/basics/buttons/Buttons";
import searchIcon from "../../../../assets/img/icon-back-grey.png"

const screenWidth = 0.9*Dimensions.get('window').width;

const RegistrationDetailPage = () => {
    const theme = useTheme()
    const styles = styling(theme)
    const {registrant} = useRegistrationDetail() 
    return (
        <MainContainer>
            <View style={[styles.container]} >
            <View style={[{width: "100%", flexDirection:'row', alignItems:'center', height:'auto'},styles.mb8]}>
                <ButtonIconTransparent>
                    <Image source={searchIcon} style={{height: 32, width: 32}} />
                </ButtonIconTransparent>
                <Text style={[theme.text.h5Type, {color: theme.color.grey, marginLeft:8}]}>Registration Detail</Text>
            </View>
                <ScrollView contentContainerStyle={{width:screenWidth, paddingBottom:40, paddingTop:12}}>
                    <View style={[styles.detailContainer, styles.mb24,]}>
                        <View style={styles.containerInside}>
                            <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Customer Name</Text>
                            <Text style={[theme.text.body1SemiboldType, {color: theme.color.grey}, styles.mb8]}>STAR Sharia Money Market</Text>
                            <Text style={[theme.text.labelInput, {color: theme.color.extraLightGrey}, styles.mb24]}>SID: <Text style={[{color: theme.color.orangePrimary}]}>1122334455667788 </Text></Text>
                            <View style={styles.containerBadge}>
                                <Text style={[theme.text.labelInput, {color: theme.color.extraLightGrey}]}>
                                    Updated:{' '}
                                    <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}]}>
                                        17 August 2022 14:58:55
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.flexRow, styles.marginHorizontal12, styles.mb24]}>
                        <View style={[styles.flexColumn, styles.center, styles.whiteBG, styles.padding16, styles.marginRight6, styles.cardElevation, styles.borderRadius10]}>
                            <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Reg. Status</Text>
                            <View style={[styles.flexColumn, styles.center, styles.redBadge]}>
                                <Text style={{color: '#A82525'}}>Rejected</Text>
                            </View>
                        </View>
                        <View style={[styles.flexColumn, styles.center, styles.whiteBG, styles.padding16, styles.marginLeft6, styles.cardElevation, styles.borderRadius10]}>
                            <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>Comp. Status</Text>
                            <View style={[styles.flexColumn, styles.center, styles.greenBadge]}>
                                <Text style={{color: '#21943A'}}>Completed</Text>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.flexColumn, styles.whiteBG, styles.padding16, styles.cardElevation, styles.borderRadius10, styles.marginHorizontal12]}>
                        <Text style={[theme.text.body2SemiboldType, {color: theme.color.lightGrey}, styles.mb24]}>
                            More Information
                        </Text>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>
                            IFUA
                        </Text>
                        <Text style={[theme.text.body2Type, {color: theme.color.grey}, styles.mb24]}>
                            Dummy IFUA
                        </Text>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>
                            Created Time
                        </Text>
                        <Text style={[theme.text.body2Type, {color: theme.color.grey}, styles.mb24]}>
                            15 August 2022 12:00:52
                        </Text>
                        <Text style={[theme.text.labelInput, {color: theme.color.lightGrey}, styles.mb8]}>
                            Rejection Note
                        </Text>
                        <Text style={[theme.text.body2Type, styles.mb8,{
                            color: theme.color.grey, 
                            backgroundColor:'rgba(58, 53, 65, 0.08)', 
                            borderRadius: 10,
                            padding: 16,
                        }]}>
                            No note to be displayed
                        </Text>
                    </View>
                </ScrollView>
            </View>
        </MainContainer>
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
        height: '100%',
        marginTop: 40,
    },
    containerInside: {
        flexDirection: "column",
        alignItems: "flex-start",
        width: "auto"
    },
    containerBadge: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: theme.color.orangeExtraLight,
        paddingVertical: 8,
        borderRadius: 50
    },
    detailContainer: {
        marginHorizontal: 12,
        display: "flex",
        flexDirection: "column",
        padding: 16,
        backgroundColor: theme.color.white,
        borderRadius: 10,
        alignSelf: 'stretch',

        shadowColor: 'rgba(133, 133, 133, 1)',
        shadowOffset: {width: 6, height: 6},
        elevation: 15
    },
    titleContainer: {
        marginBottom: 32,
        alignItems: 'center',
        display: "flex"
    },
    mb8:{
        marginBottom: 8,
    },
    mb16:{
        marginBottom: 16,
    },
    mb32:{
        marginBottom: 32,
    },
    mb24:{
        marginBottom: 24,
    },
    padding16:{
        padding:16
    },
    marginHorizontal12: {
        marginHorizontal: 12
    },
    marginLeft6:{
        marginLeft: 6
    },
    marginRight6:{
        marginRight: 6
    },
    borderRadius10: {
        borderRadius: 10
    },
    cardElevation: {
        shadowColor: '#D3D3D3',
        shadowOffset: {width: 4, height: 4},
        elevation: 15
    },
    flexRow: {
        flex: 1,
        flexDirection: 'row'
    },
    flexColumn: {
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    whiteBG:{
        backgroundColor: 'white'
    },
    greenBadge: {
        backgroundColor: '#DCFBE3',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    redBadge: {
        backgroundColor: '#FBDCDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    blueBadge: {
        backgroundColor: '#ECF6FF',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    },
    yellowBadge: {
        backgroundColor: '#FBEDDC',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10
    }
})

export default RegistrationDetailPage