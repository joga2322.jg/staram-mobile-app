export const theme = {
    color: {
        grey: 'rgba(58, 53, 65, 0.87)',
        lightGrey: 'rgba(58, 53, 65, 0.68)',
        extraLightGrey: 'rgba(58, 53, 65, 0.38)',
        orangePrimary: '#E65525',
        orangeSecondary: '#FF7344',
        orangeExtraLight: '#FFECE6',
        errorMedium: '#CA2F54',
        errorLight: '#EF748A',
        errorExtraLight: '#FEE4EA',
        white: 'white',
        bgColor: '#FAFAFA',
        greenBadge: '#21943A',
        yellowBadge: '#AC7A2F',
        redBadge: '#A82525',
        blueBadge: '#28A7DF'
    },
    text: {
        h3Type: {
            fontStyle: 'normal',
            fontSize: 28,
            lineHeight: 42,
            letterSpacing: 0.25,
            fontFamily: 'Montserrat-Bold'
        },
        h4Type: {
            fontStyle: 'normal',
            fontSize: 24,
            lineHeight: 42,
            letterSpacing: 0.25,
            fontFamily: 'Montserrat-Bold'
        },
        h5Type: {
            fontStyle: 'normal',
            fontSize: 20,
            lineHeight: 36,
            letterSpacing: 0.25,
            fontFamily: 'Montserrat-Bold'
        },
        h6Type: {
            fontStyle: 'normal',
            fontSize: 16,
            lineHeight: 24,
            letterSpacing: 0.25,
            fontFamily: 'Montserrat-Bold'
        },
        body1Type: {
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 16,
            lineHeight: 24,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-Medium'
        },
        body1SemiboldType: {
            fontStyle: 'normal',
            fontWeight: '600',
            fontSize: 16,
            lineHeight: 24,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-SemiBold'
        },
        body2Type: {
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 14,
            lineHeight: 20,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-Medium'
        },
        body2SemiboldType: {
            fontStyle: 'normal',
            fontWeight: '600',
            fontSize: 14,
            lineHeight: 17,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-SemiBold'
        },
        body3Type: {
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 13,
            lineHeight: 18,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-Medium'
        },
        body3SemiboldType: {
            fontStyle: 'normal',
            fontWeight: '600',
            fontSize: 13,
            lineHeight: 18,
            letterSpacing: 0.15,
            fontFamily: 'Montserrat-Medium'
        },
        labelInput: {
            color: 'rgba(61, 61, 61, 1)',
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 12,
            fontFamily: 'Montserrat-Medium'
        },
        hintType: {
            fontStyle: 'normal',
            fontWeight: '400',
            fontSize: 10,
            lineHeight: 12,
            fontFamily: 'Montserrat-Medium'
        },
        hintNormalType: {
            fontStyle: 'normal',
            fontWeight: '500',
            fontSize: 11,
            lineHeight: 12,
            fontFamily: 'Montserrat-Medium'
        },
        labelButton: {
            fontStyle: 'normal',
            fontWeight: '600',
            fontSize: 14,
            lineHeight: 20,
            color: 'white',
            fontFamily: 'Montserrat-Medium'
        }
    },
    layout: {
        mb6: {
            marginBottom: 6
        },
        mb8:{
            marginBottom: 8,
        },
        mb16: {
            marginBottom: 16
        },
        mb24:{
            marginBottom: 24,
        },
        mb32:{
            marginBottom: 32,
        },
        mh12: {
            marginHorizontal: 12
        },
        mv24: {
            marginVertical: 24
        },
        ml6:{
            marginLeft: 6
        },
        mr6:{
            marginRight: 6
        },
        p16:{
            padding:16
        },
    }
}