import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from "@react-navigation/native";
import DashboardPage from '../../features/Dashboard/DashboardPage';
import ProductListPage from "../../features/Product/ProductList/ProductListPage";
import RegistrationListPage from "../../features/Registration/RegistrationList/RegistrationListPage";
import dashActive from "../../../assets/img/icon-dashboard.png"
import iconFund from "../../../assets/img/icon-mutual-fund.png"
import iconREG from "../../../assets/img/icon-register.png"
import { Image } from "react-native";


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const DashboardStack = () => {
    return (
        <Stack.Navigator
          initialRouteName="Dashboard"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="Dashboard"
            component={DashboardPage}
          />
          <Stack.Screen
            name="Products"
            component={ProductListPage}
          />
          <Stack.Screen
            name="Registrations"
            component={RegistrationListPage}
          />
        </Stack.Navigator>
      );
}

const ProductsStack = () => {
    return (
        <Stack.Navigator
          initialRouteName="Products"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="Dashboard"
            component={DashboardPage}
          />
          <Stack.Screen
            name="Products"
            component={ProductListPage}
          />
          <Stack.Screen
            name="Registrations"
            component={RegistrationListPage}
          />
        </Stack.Navigator>
      );
}

const RegistrationsStack = () => {
    return (
        <Stack.Navigator
          initialRouteName="Registrations"
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="Dashboard"
            component={DashboardPage}
          />
          <Stack.Screen
            name="Products"
            component={ProductListPage}
          />
          <Stack.Screen
            name="Registrations"
            component={RegistrationListPage}
          />
        </Stack.Navigator>
      );
}

const BottomNavbar = () => {
    return ( 
        <NavigationContainer>
            <Tab.Navigator
            initialRouteName="Dashboard"
            screenOptions={{
                headerShown: false,
                "tabBarActiveTintColor": "#E65525",
                tabBarInactiveTintColor: 'rgba(58, 53, 65, 0.38)',
                "tabBarStyle": [
                    {
                    "display": "flex",
                    paddingBottom: 12,
                    height: 68
                    },
                    null
                ]
            }}
            >
            <Tab.Screen
                name="DashboardStack"
                component={DashboardStack}
                options={{
                tabBarLabel: 'Dashboard',
                tabBarIcon: (focused) => {
                    let color = focused ? "#E65525" : "#8e8e93"
                    return <Image source={dashActive} style={{width:20, height:20}}/>
                },
                }}
            />
            <Tab.Screen
                name="ProductsStack"
                component={ProductsStack}
                options={{
                tabBarLabel: 'Products',
                tabBarIcon: (focused) => {
                    let color = focused ? "#E65525" : "#ffffff"
                    return <Image source={iconFund} style={{width:20, height:20}}/>
                },
                }}
            />
            <Tab.Screen
                name="RegistrationsStack"
                component={RegistrationsStack}
                options={{
                tabBarLabel: 'Registration',
                tabBarIcon: (focused) => {
                    let color = focused ? "#E65525" : "#ffffff"
                    return <Image source={iconREG} style={{width:20, height:20}}/>
                },
                }}
            />
            </Tab.Navigator>
        </NavigationContainer>
    );
}
 
export default BottomNavbar;