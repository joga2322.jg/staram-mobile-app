import { useState } from "react"
import { Pressable, StyleSheet, Text, TextInput, View } from "react-native"
import { Entypo } from '@expo/vector-icons'; 
import { useTheme } from "../../../shared/context/ThemeContext";

export const AppTextInput = ({label, value, onChangeValue, placeholder}) => {
    const theme = useTheme();
    const styles = styling(theme)
    return (
        <View>
            <Text style={theme.text.labelInput}>{label}</Text>
            <TextInput
                placeholder={placeholder}
                onChangeText={onChangeValue}
                value={value}
                keyboardType="default"
                style={styles.input}
            />
        </View>
    )
}

export const AppPasswordInput = ({value, onChangeValue, placeholder}) => {
    const theme = useTheme();
    const styles = styling(theme)
    const [hidePass, setHidePass] = useState(true)

    return (
        <>
            <Text style={theme.text.labelInput}>Password</Text>
            <View style={[styles.input, styles.passwordInput]}>
                <TextInput
                    secureTextEntry={hidePass}
                    onChangeText={onChangeValue}
                    value={value}
                    placeholder={placeholder}
                    style={{fontFamily: 'Montserrat-Medium', width: '90%'}}
                />
                <Pressable onPress={() => setHidePass(!hidePass)}>
                    <Entypo name={hidePass ? "eye-with-line" : "eye"} size={16} color="rgba(61, 61, 61, 1)" />
                </Pressable>
            </View>
        </>
    )
}

export const SearchInput = ({value, onChangeValue, placeholder}) => {
    const theme = useTheme();
    const styles = styling(theme)
    return (
        <TextInput
            keyboardType="default"
            style={styles.searchInput}
            value={value}
            onChangeText={onChangeValue}
            placeholder={placeholder}
        />
    )
}

const styling = (theme) => StyleSheet.create({
    input: {
        display: 'flex',
        marginTop: 12,
        paddingHorizontal: 16,
        paddingVertical: 10,
        width: '100%',
        height: 'auto',
        borderColor: '#BDBDBD',
        borderStyle: 'solid',
        borderWidth: 1.4,
        borderRadius: 10,

        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 20,
        color: '#6D6D6D',
        fontFamily: 'Montserrat-Medium',

        backgroundColor: 'white'
    },
    passwordInput: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    searchInput: {
        borderBottomColor: theme.color.extraLightGrey,
        borderBottomWidth: 1,
        margin: 12,
        height: 40,
        width: '70%',
        color: '#6D6D6D',
        fontFamily: 'Montserrat-Medium',
    }
})