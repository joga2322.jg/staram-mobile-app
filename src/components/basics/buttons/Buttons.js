import { StyleSheet, Text, TouchableOpacity } from "react-native"
import { useTheme } from "../../../shared/context/ThemeContext"

export const ButtonPrimary = ({label, onClick}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <TouchableOpacity onPress={onClick} style={styles.buttonContainer}>
            <Text style={theme.text.labelButton}>{label}</Text>
        </TouchableOpacity>
    )
}

export const ButtonIcon = ({onClick, children}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <TouchableOpacity onPress={onClick} style={styles.iconContainer}>
            {children}
        </TouchableOpacity>
    )
}

export const ButtonIconTransparent = ({onClick, children}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <TouchableOpacity onPress={onClick} style={styles.iconContainerTransparent}>
            {children}
        </TouchableOpacity>
    )
}

const styling = (theme) => StyleSheet.create({
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 12,
        backgroundColor: theme.color.orangePrimary,
        borderRadius: 10
    },
    iconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 8,
        height: '70%',
        backgroundColor: theme.color.orangePrimary,
        borderRadius: 10
    },
    iconContainerTransparent: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8,
        height: 'auto',
        borderRadius: 10
    }
})