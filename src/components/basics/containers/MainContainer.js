import { StatusBar } from "expo-status-bar"
import { SafeAreaView, StyleSheet } from "react-native"
import { useTheme } from "../../../shared/context/ThemeContext"

const MainContainer = ({children}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar style="auto" />
            {children}
        </SafeAreaView>
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.color.bgColor,
        justifyContent: 'flex-start',
        padding: 20
    }
})

export default MainContainer