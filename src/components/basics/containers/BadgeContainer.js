import { StyleSheet, Text, View } from 'react-native'
import { useTheme } from '../../../shared/context/ThemeContext'

export const BadgeContainer = ({additionalStyle, children}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <View style={[styles.container, additionalStyle]}>
            {children}
        </View>
    )
}

export const MiniBadge = ({type, label, additionalStyle = {}}) => {
    const theme = useTheme()
    const styles = styling(theme)

    return (
        <>
            {type==="danger" &&
                <View style={[styles.badge, styles.redBadge, additionalStyle]}>
                    <Text style={[theme.text.labelInput, styles.redColor]}>{label}</Text>
                </View>
            }
            {type==="success" &&
                <View style={[styles.badge, styles.greenBadge, additionalStyle]}>
                    <Text style={[theme.text.labelInput, styles.greenColor]}>{label}</Text>
                </View>
            }
            {type==="warning" &&
                <View style={[styles.badge, styles.yellowBadge, additionalStyle]}>
                    <Text style={[theme.text.labelInput, styles.yellowColor]}>{label}</Text>
                </View>
            }
            {type==="primary" &&
                <View style={[styles.badge, styles.blueBadge, additionalStyle]}>
                    <Text style={[theme.text.labelInput, styles.blueColor]}>{label}</Text>
                </View>
            }
        </>
    )
}

const styling = (theme) => StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: theme.color.orangeExtraLight,
        paddingVertical: 8,
        borderRadius: 50
    },
    badge: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-start',
        paddingVertical: 8,
        paddingHorizontal: 16,
        borderRadius: 10,
    },
    redBadge: {
        backgroundColor: '#FBDCDC',
    },
    redColor: {
        color: '#A82525'
    },
    greenBadge: {
        backgroundColor: '#DCFBE3',
    },
    greenColor: {
        color: '#21943A'
    },
    yellowBadge: {
        backgroundColor: '#FBEDDC',
    },
    yellowColor: {
        color: '#FCA63D'
    },
    blueBadge: {
        backgroundColor: '#ECF6FF',
    },
    blueColor: {
        color: '#2D99FA'
    }
})