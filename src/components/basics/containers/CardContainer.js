import { StyleSheet, View } from "react-native"
import { useTheme } from "../../../shared/context/ThemeContext"

const CardContainer = ({additionalStyle, children}) => {
    const theme = useTheme()
    const styles = styling(theme)
    return (
        <View style={[styles.card, additionalStyle]}>
            {children}
        </View>
    )
}

const styling = (theme) => StyleSheet.create({
    card: {
        display: "flex",
        flexDirection: "column",
        backgroundColor: theme.color.white,
        borderRadius: 10,

        shadowColor: '#A9A9A9',
        shadowOffset: {width: 4, height: 4},
        elevation: 15
    }
})

export default CardContainer
